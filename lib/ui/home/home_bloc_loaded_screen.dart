import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:majootestcase/bloc/auth_bloc/auth_bloc_cubit.dart';
import 'package:majootestcase/bloc/home_bloc/home_bloc_cubit.dart';
import 'package:majootestcase/common/widget/appbar_widget.dart';
import 'package:majootestcase/models/movie_model.dart';
import 'package:majootestcase/ui/home/home_detail_screen.dart';
import 'package:majootestcase/utils/dimension.dart';

class HomeBlocLoadedScreen extends StatelessWidget {
   final List<Results> data;

  const HomeBlocLoadedScreen({Key? key, required this.data}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBarWidget.create(context,title: 'Movies',isNavigation: false,actions: [
        IconButton(onPressed: ()=> context.read<AuthBlocCubit>().handleLogout(),
            icon: Icon(Icons.logout))
      ]),
      backgroundColor: Colors.grey.withOpacity(0.9),
      body: ListView.builder(
        padding: EdgeInsets.all(20),
          shrinkWrap: true,
          itemCount: data.length,
          itemBuilder: (context, index) {
            return Padding(
              padding: const EdgeInsets.all(10),
              child: movieItemWidget(context,data[index]),
            );
        },
      ),
    );
  }
  
  void _handleToDetailMovies(BuildContext context,Results results){
    Navigator.push(context, MaterialPageRoute(
        builder: (context) => HomeDetailScreen(detailMovies: results))
    );
  }
  
  String _titleMovies(Results data){
    String title = data.title?.isEmpty ?? false ? data.originalTitle ?? '' : data.title ?? '';
    return title.isEmpty ? data.originalName ?? '' : title;
  }

  Widget movieItemWidget(BuildContext context,Results data){
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        ClipRRect(
          borderRadius: BorderRadius.circular(15),
          child: Stack(
            children: [
              Image.network("https://image.tmdb.org/t/p/w500/${data.posterPath}",width: screenWidth(context,dividedBy: 2)),
              Positioned(
                bottom: -5,
                right: 10,
                child: TextButton(
                  onPressed: ()=> _handleToDetailMovies(context, data),
                  child: Text('see detail movie',style: TextStyle(color: Colors.white70,fontSize: 12),),
                )
              ),
            ],
          ),
        ),
        SizedBox(width: 10,),
        Flexible(child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(_titleMovies(data),style: TextStyle(color: Colors.greenAccent,fontSize: 16)),
            SizedBox(height: 10),
            Text(data.overview ?? '',style: TextStyle(color: Colors.white,fontSize: 12),maxLines: 5,overflow: TextOverflow.ellipsis,),
            SizedBox(height: 20),
            Text('Popularity : ${data.popularity} users',style: TextStyle(color: Colors.yellow,fontSize: 14)),
          ],
        ))
      ],
    );
  }
}
