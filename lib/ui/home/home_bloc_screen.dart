import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:majootestcase/common/widget/custom_button.dart';
import 'package:majootestcase/ui/initial/initial_page.dart';
import '../../bloc/home_bloc/home_bloc_cubit.dart';
import 'home_bloc_loaded_screen.dart';
import '../extra/loading.dart';
import '../extra/error_screen.dart';

class HomeBlocScreen extends StatelessWidget {
  const HomeBlocScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<HomeBlocCubit, HomeBlocState>(
        builder: (context, state) {
        if(state is HomeBlocLoadedState) {
          return HomeBlocLoadedScreen(data: state.data ?? []);
        } else if(state is HomeBlocLoadingState) {
          return LoadingIndicator();
        } else if(state is HomeBlocInitialState) {
          return InitialScreen();
        } else if(state is HomeBlocErrorState) {
          return ErrorScreen(
            retry: (){},
            retryButton: CustomButton(
              leadingIcon: Icon(Icons.network_check),
              onPressed: ()=> context.read<HomeBlocCubit>().handleConnection(),
              isSecondary: true,
              text: 'Refresh',
            ),
            message: state.error
          );
        } return Center(child: Text(
        kDebugMode?"state not implemented $state": ""
      ));
    });
  }
}
