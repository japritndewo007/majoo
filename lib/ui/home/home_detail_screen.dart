import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:majootestcase/models/movie_model.dart';
import 'package:majootestcase/utils/helper.dart';

import '../../common/widget/appbar_widget.dart';
import '../../utils/dimension.dart';

class HomeDetailScreen extends StatelessWidget {
  final Results detailMovies;
  const HomeDetailScreen({Key? key, required this.detailMovies}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBarWidget.create(context,title: '${_titleMovie(detailMovies)}'),
      body: Padding(
        padding: const EdgeInsets.all(20),
        child: movieDetailWidget(context, detailMovies),
      ),
    );
  }

  String _titleMovie(Results data){
    String title = data.title?.isEmpty ?? false ? data.originalTitle ?? '' : data.title ?? '';
    return title.isEmpty ? data.originalName ?? '' : title;
  }

  String _dateMovie(Results data){
    String date = data.releaseDate ?? '';
    DateFormat format = DateFormat("yyyy-MM-dd");
    String convertedDate = Converter.convertDate(format.parse(date.isEmpty ? data.firstAirDate ?? '' : date), 'MMMM yyyy');
    return convertedDate;
  }

  Widget footerWidget(Results data){
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text('Popularity : ${data.popularity} users',
            style: TextStyle(color: Colors.blue,fontSize: 14,fontWeight: FontWeight.bold)),
        SizedBox(height: 20,),
        Row(
          children: [
            Text('${data.voteAverage}',style: TextStyle(color: Colors.blue,fontSize: 30,fontWeight: FontWeight.bold)),
            Icon(Icons.star,color: Colors.orange,size: 30),
          ],
        )
      ],
    );
  }

  Widget movieDetailWidget(BuildContext context,Results data){
    return Column(
      children: [
        SizedBox(height: 20,),
        ClipRRect(
            borderRadius: BorderRadius.circular(15),
            child: Image.network("https://image.tmdb.org/t/p/w500/${data.posterPath}",height: screenHeight(context,dividedBy: 2)),
        ),
        SizedBox(height: 10),
        Text(_dateMovie(data),style: TextStyle(color: Colors.black,fontSize: 16,fontWeight: FontWeight.bold)),
        SizedBox(height: 20),
        Text(data.overview ?? '',style: TextStyle(color: Colors.black,fontSize: 12),textAlign: TextAlign.justify,),
        SizedBox(height: 20),
        footerWidget(data)
      ],
    );
  }
}
