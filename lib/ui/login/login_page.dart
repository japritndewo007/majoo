import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:majootestcase/bloc/auth_bloc/auth_bloc_cubit.dart';
import 'package:majootestcase/bloc/home_bloc/home_bloc_cubit.dart';
import 'package:majootestcase/common/widget/custom_button.dart';
import 'package:majootestcase/common/widget/snackbar.dart';
import 'package:majootestcase/common/widget/text_form_field.dart';
import 'package:majootestcase/models/user.dart';
import 'package:majootestcase/utils/dimension.dart';
import 'package:majootestcase/utils/helper.dart';



class LoginPage extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<LoginPage> {
  late final _userNameController;
  late final _emailController;
  late final _passwordController;
  GlobalKey<FormState> formKey = new GlobalKey<FormState>();

  bool _isObscurePassword = true;
  bool _isFetching = false;
  bool _showRegisterForm = false;

  @override
  void initState() {
    _userNameController = TextController();
    _emailController = TextController();
    _passwordController = TextController();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<AuthBlocCubit, AuthBlocState>(
      listener: (context,state){
        if (state is AuthBlocErrorState){
          _isFetching = false;
          showSnackBar(context, message: state.error);
        }
        if (state is AuthBlocLoadingState){
          _isFetching = true;
        }
        if(state is AuthBlocLoggedInState){
          _isFetching = false;
          showSnackBar(context, message: 'Login Berhasil',isSuccessState: true);
        }
        if (state is AuthBlocSuccesState){
          _showRegisterForm = false;
          showSnackBar(context, message: 'Register Berhasil',isSuccessState: true);
        }
      },
      child: Scaffold(
        body: ListView(
          padding: EdgeInsets.fromLTRB(25, screenHeight(context,dividedBy: 5), 25, 0),
          children: [
            Text(
              'Selamat Datang',
              textAlign: TextAlign.center,
              style: TextStyle(
                fontSize: 24,
                fontWeight: FontWeight.bold,
                // color: colorBlue,
              ),
            ),
            SizedBox(height: 10),
            Text(
              _showRegisterForm ? 'Silahkan Isi Register Form dibawah' : 'Silahkan login terlebih dahulu',
              textAlign: TextAlign.center,
              style: TextStyle(
                fontSize: 15,
                fontWeight: FontWeight.w400,
              ),
            ),
            SizedBox(
              height: 20,
            ),
            Container(
              padding: EdgeInsets.all(10),
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(10),
                boxShadow: [BoxShadow(color: Colors.black54.withOpacity(0.15),
                    blurRadius: 10,offset: Offset(0,3),
                    spreadRadius: 5)],),
              child: _loginForm(),
            ),
            SizedBox(
              height: 30,
            ),
            CustomButton(
              text: _showRegisterForm ? 'Register' : 'Login',
              leadingIcon: _isFetching ? SizedBox(height: 20,width: 20,child: CircularProgressIndicator(color: Colors.white),) : SizedBox(),
              onPressed: _showRegisterForm ? handleRegister : handleLogin,
            ),
            _register(),
          ],
        ),
      ),
    );
  }

  Widget _loginForm() {
    return Form(
      key: formKey,
      child: Column(
        children: [
          _showRegisterForm ? Column(
            children: [
              CustomTextFormField(
                context: context,
                controller: _userNameController,
                hint: 'username',
                label: 'Username',
              ),
              SizedBox(height: 20,),
            ],
          ) : SizedBox(),
          CustomTextFormField(
            context: context,
            controller: _emailController,
            isEmail: true,
            hint: 'Example@123.com',
            label: 'Email',
            validator: (val) {
              if (val != null)
                return Validate.email(val) ? null : 'email is invalid';
              return null;
            },
          ),
          SizedBox(height: 20,),
          CustomTextFormField(
            context: context,
            label: 'Password',
            hint: 'password',
            controller: _passwordController,
            isObscureText: _isObscurePassword,
            suffixIcon: IconButton(
              icon: Icon(
                _isObscurePassword
                    ? Icons.visibility_off_outlined
                    : Icons.visibility_outlined,
              ),
              onPressed: () {
                setState(() {
                  _isObscurePassword = !_isObscurePassword;
                });
              },
            ),
          ),
        ],
      ),
    );
  }

  Widget _register() {
    return Align(
      alignment: Alignment.center,
      child: TextButton(
        onPressed: ()=> setState(() => _showRegisterForm = !_showRegisterForm),
        child: RichText(
          text: TextSpan(
              text: _showRegisterForm ? 'Kembali ke ' : 'Belum punya akun? ',
              style: TextStyle(color: Colors.black45),
              children: [
                TextSpan(
                  text: _showRegisterForm ? 'Login' : 'Daftar',
                ),
              ]),
        ),
      ),
    );
  }

  bool _isFormEmpty(){
    return _emailController.value.toString().isEmpty && _passwordController.value.toString().isEmpty;
  }

  bool _isEmailValid(){
    return Validate.email(_emailController.value);
  }

  void handleLogin() async {
    final _email = _emailController.value;
    final _password = _passwordController.value;
    final _userName = _userNameController.value;
    if (_isFormEmpty()){
      showSnackBar(context, message: 'Form tidak boleh kosong, mohon cek kembali data yang anda inputkan');
      formKey.currentState?.validate();
      return;
    }
    if (!_isEmailValid()){
      showSnackBar(context, message: 'Masukkan e-mail yang valid');
      formKey.currentState?.validate();
      return;
    }
    if (formKey.currentState?.validate() == true &&
        _email != null &&
        _password != null
       ) {
      User user = User(
          email: _email,
          password: _password.toString(),
          userName: _userName
      );
      context.read<AuthBlocCubit>().loginUser(user);
    }
  }

  void handleRegister() async {
    final _email = _emailController.value;
    final _password = _passwordController.value;
    final _userName = _userNameController.value;
    if (_isFormEmpty()){
      showSnackBar(context, message: 'Form tidak boleh kosong, mohon cek kembali data yang anda inputkan');
      formKey.currentState?.validate();
      return;
    }
    if (!_isEmailValid()){
      showSnackBar(context, message: 'Masukkan e-mail yang valid');
      formKey.currentState?.validate();
      return;
    }
    if (formKey.currentState?.validate() == true &&
        _email != null &&
        _password != null
    ) {
      User user = User(
        email: _email,
        password: _password.toString(),
        userName: _userName
      );
      context.read<AuthBlocCubit>().registerUser(user);
    }
  }
}
