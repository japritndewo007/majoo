import 'package:flutter/material.dart';

import '../../utils/dimension.dart';

class InitialScreen extends StatelessWidget {
  const InitialScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        height: screenHeight(context),
        width: screenWidth(context),
        child: Center(
          child: CircularProgressIndicator(color: Colors.blue,),
        ),
      ),
    );
  }
}
