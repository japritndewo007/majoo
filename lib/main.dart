import 'package:majootestcase/bloc/blocs.dart';
import 'package:majootestcase/ui/home/home_bloc_screen.dart';
import 'package:majootestcase/ui/initial/initial_page.dart';
import 'package:majootestcase/ui/login/login_page.dart';
import 'package:flutter/foundation.dart';
import 'bloc/auth_bloc/auth_bloc_cubit.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
        providers: blocList,
        child: MaterialApp(
        title: 'Flutter Demo',
        theme: ThemeData(
          primarySwatch: Colors.blue,
          visualDensity: VisualDensity.adaptivePlatformDensity,
        ),
        home: MyHomePageScreen(),
      ));
  }
}

class MyHomePageScreen extends StatelessWidget {
  const MyHomePageScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<AuthBlocCubit, AuthBlocState>(
        builder: (context, state) {
          if (state is AuthBlocInitialState){
            return InitialScreen();
          } else if(state is AuthBlocLoginState ||
              state is AuthBlocLoadingState ||
              state is AuthBlocErrorState ||
              state is AuthBlocSuccesState) {
            return LoginPage();
          } else if(state is AuthBlocLoggedInState ) {
            return HomeBlocScreen();
          }
          return Center(child: Text(
              kDebugMode?"state not implemented $state": ""
          ));
        });
  }
}

