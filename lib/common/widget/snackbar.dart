import 'package:flutter/material.dart';

showSnackBar(BuildContext context,{required String message, bool isSuccessState = false}){
  ScaffoldMessenger.of(context).showSnackBar(
    SnackBar(
      content: Text(message,
        style: const TextStyle(color: Colors.white),
      ),
      backgroundColor: isSuccessState ? Colors.blue : Colors.red,
      behavior: SnackBarBehavior.floating,
    ),
  );
}