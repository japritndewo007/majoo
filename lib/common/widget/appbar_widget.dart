import 'package:flutter/material.dart';

class AppBarWidget {
  static AppBar create(BuildContext context, {String? title, List<Widget>? actions, Widget? leading, bool isNavigation = true}) {
    return AppBar(
      backgroundColor: Colors.grey,
      title: title != null ? Text(title,style: TextStyle(
          fontSize: 18,
          color: Colors.white
      ),) : SizedBox(),
      elevation: 0,
      actions: actions,
      leading: isNavigation ? leading ??
          BackButton(
            color: Colors.white,
          ) : const SizedBox(),
      centerTitle: true,
    );
  }
}