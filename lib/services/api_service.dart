import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:majootestcase/models/movie_model.dart';

import 'package:majootestcase/services/dio_config_service.dart' as dioConfig;

class ApiServices{

  Future<MovieModel> getMovieList() async {
    try {
      var dio = await dioConfig.dio();
      var apiKey = '0bc9e6490f0a9aa230bd01e268411e10';
      Response? response = await dio?.get('/3/trending/all/day',queryParameters: {
        'api_key' : apiKey
      });
      return MovieModel.fromJson(response?.data);
    } catch(e) {
      throw e.toString();
    }
  }
}