import 'dart:convert';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:majootestcase/models/user.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../utils/helper.dart';

part 'auth_bloc_state.dart';

class AuthBlocCubit extends Cubit<AuthBlocState> {
  AuthBlocCubit() : super(AuthBlocInitialState());

  void fetchHistoryLogin() async{
    try {
      emit(AuthBlocInitialState());
      SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
      bool? isLoggedIn = sharedPreferences.getBool("is_logged_in");
      if(isLoggedIn== null){
        emit(AuthBlocLoginState());
      }else{
        if(isLoggedIn){
          emit(AuthBlocLoggedInState());
        }else{
          emit(AuthBlocLoginState());
        }
      }
    } catch (e) {
      emit(AuthBlocErrorState(e.toString()));
    }
  }

  void handleLogout() async {
    try {
      SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
      sharedPreferences.remove('is_logged_in');
      emit(AuthBlocLoginState());
    } catch (e) {
      emit(AuthBlocErrorState(e.toString()));
    }
  }

  void registerUser(User user) async {
    try {
      SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
      sharedPreferences.setString("user_value", userToJson(user));
      emit(AuthBlocSuccesState());
    } catch (e) {
      emit(AuthBlocErrorState(e.toString()));
    }
  }

  void loginUser(User user) async{
    try {
      SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
      emit(AuthBlocLoadingState());
      String userPrefs = sharedPreferences.getString('user_value') ?? '';
      if (userPrefs.isNotEmpty){
        User userExisting = userFromJson(userPrefs);
        if (userExisting.email != user.email && userExisting.password != user.password){
          emit(AuthBlocErrorState('Login gagal , periksa kembali inputan anda'));
        } else {
          await sharedPreferences.setBool("is_logged_in",true);
          sharedPreferences.setString("user_value", userToJson(user));
          emit(AuthBlocLoggedInState());
        }
      } else {
        emit(AuthBlocErrorState('Silahkan Daftar terlebih dahulu'));
      }

    } catch (e) {
      emit(AuthBlocErrorState(e.toString()));
    }
  }

}
