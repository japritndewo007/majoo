import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:majootestcase/bloc/auth_bloc/auth_bloc_cubit.dart';
import 'package:majootestcase/bloc/home_bloc/home_bloc_cubit.dart';

final List<BlocProvider> blocList = [
  BlocProvider<AuthBlocCubit>(create: (_) => AuthBlocCubit()..fetchHistoryLogin()),
  BlocProvider<HomeBlocCubit>(create: (_) => HomeBlocCubit()..handleConnection()),
];