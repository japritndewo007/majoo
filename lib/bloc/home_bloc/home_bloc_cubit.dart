import 'package:bloc/bloc.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:equatable/equatable.dart';
import 'package:majootestcase/models/movie_model.dart';

import 'package:majootestcase/services/api_service.dart';

part 'home_bloc_state.dart';

class HomeBlocCubit extends Cubit<HomeBlocState> {
  HomeBlocCubit() : super(HomeBlocInitialState());
  ApiServices apiServices = ApiServices();

  void fetchingData() async {
    try {
      emit(HomeBlocInitialState());
      MovieModel movieResponse = await apiServices.getMovieList();
      emit(HomeBlocLoadedState(movieResponse.results));
    } catch (e) {
      emit(HomeBlocErrorState(e.toString()));
    }
  }

  void handleConnection() async {
    try {
      var connectivityResult = await (Connectivity().checkConnectivity());
      if (connectivityResult == ConnectivityResult.none){
        emit(HomeBlocErrorState('Periksa Kembali Koneksi Internet Anda'));
      } else {
        fetchingData();
      }
    } catch (e) {
      emit(HomeBlocErrorState(e.toString()));
    }
  }

}
