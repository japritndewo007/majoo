import 'dart:convert';

User userFromJson(String str) => User.fromJson(json.decode(str));

String userToJson(User data) => json.encode(data.toJson());

class User {
  User({
    this.email,
    this.password,
    this.userName,
  });

  final String? email;
  final String? password;
  final String? userName;

  User copyWith({
    String? email,
    String? password,
    String? userName,
  }) =>
      User(
        email: email ?? this.email,
        password: password ?? this.password,
        userName: userName ?? this.userName,
      );

  factory User.fromJson(Map<String, dynamic> json) => User(
    email: json["email"],
    password: json["password"],
    userName: json["username"],
  );

  Map<String, dynamic> toJson() => {
    "email": email,
    "password": password,
    "username": userName,
  };
}
