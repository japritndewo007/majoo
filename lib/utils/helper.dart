import 'constant.dart';
import 'package:intl/intl.dart';

class Validate {
  static bool email(String value) {
    RegExp regex = new RegExp(RegExPattern.email);
    return regex.hasMatch(value);
  }
}

class Converter {
  static String convertDate(DateTime date, String dateFormat) {
    return DateFormat(dateFormat).format(date);
  }
}